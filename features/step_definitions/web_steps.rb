Given(/^que estou na home page$/) do
  visit('/')
end

Given(/^clico em "([^"]*)"$/) do |element|
  find(element).click
end

Given(/^digitei "([^"]*)" no campo "([^"]*)"$/) do |text, field|
  fill_in field, with: text
end

Then(/^devo ver "([^"]*)"$/) do |text|
  expect(page).to have_content(text), find('.row.busca h1').text
end


Then(/^não devo ver "Confira os XYZ livros encontrados na Estante Virtual:"$/) do
  expect(page).to have_no_content(/Confira os [\d]+ livros encontrados na Estante Virtual:/), find('.row.busca h1').text
end


