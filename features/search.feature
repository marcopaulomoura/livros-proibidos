# language: pt

Funcionalidade: Livros de venda restrita ou proibida
  Para remover livros que não podem ser vendidos
  Como o responsável pelo operações
  Eu quero saber se há livros disponíveis no site

  Esquema do Cenario: Busca por Livros de venda restrita ou proibida
    Dado que estou na home page
    E digitei "<titulo>" no campo "q"
    E clico em ".icon-search.js-search"
    Então não devo ver "Confira os XYZ livros encontrados na Estante Virtual:"
    E devo ver "Sua pesquisa não retornou nenhum resultado."

  Exemplos:
  | titulo                                 |
  | adolf hitler mein kampf                |
  | Familia Acolhedora jane valente        |
  | MEC Inep                               |
  | Coloridas de Max Pfister               |
  | Teste Não Verbal de Inteligência Geral |
