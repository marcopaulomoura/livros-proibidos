require 'capybara'
require 'capybara/cucumber'

Capybara.configure do |config|
  config.default_driver = :selenium
  config.app_host = 'http://estantevirtual.com.br'
end

if Capybara.current_driver == :selenium
  require 'headless'
  Headless.new.start
end

World(Capybara)
